#!/bin/bash




#TODO:
# UsePAM avec TOTP
# groupe autorisé ssh-users
# copie clé publique sur serveur distant
# port daemon ssh
# désactiver root












##################### GESTION D'ERREUR ####################
exec 2> /tmp/IPv7.log #rediriger la STDERR vers un fichier temporaire

function gest_err {
	#trap - ERR #désactiver la capture
	echo -e "${Trougeblanc}${Tgras}$(cat /tmp/IPv7.log)${Treset}" # afficher l'erreur
	#rm /tmp/IPv7.log #supprimer le fichier temporaire de logs
	#exit 1 #quitter le script
}

###########################################################


function modif {
	sed -i -e "s/$1/#$1/g" "$3" #commenter $1
	echo -e "$2" >> "$3" #et ajouter $2 à $3
}


lf="\n"
Trougeblanc=`tput setaf 1 && tput setab 7`
Tbleublanc=`tput setaf 4 && tput setab 7`
Tgras=`tput bold`
Treset=`tput sgr0`
clear
trap 'gest_err' ERR #capturer les signaux d'erreur, pour interrompre le script
echo -e "Merci à stribika pour son excellent article https://stribika.github.io/2015/01/04/secure-secure-shell.html$lf"
echo -e "ainsi que son wiki GitHub https://github.com/stribika/stribika.github.io/wiki/Secure-Secure-Shell$lf"
echo -e "Script par @IPv7$lf$lf"
sleep 3




echo "Installation de libpam-google-authenticator, ssh (openssh-client et openssh-server), ssh-askpass, grep et sed ..."
apt-get install -y libpam-google-authenticator ssh ssh-askpass grep sed 1> /dev/null



############# ENTROPIE #############
service haveged stop
entropieI=$(cat /proc/sys/kernel/random/entropy_avail)
echo "Votre entropie initiale est de $entropieI bits"
echo "Installation et configuration de haveged pour garantir une bonne entropie..."
apt-get install -y haveged 1> /dev/null
modif "DAEMON_ARGS" "DAEMON_ARGS=\"-w 4001\"" "/etc/default/haveged"
update-rc.d haveged defaults
service haveged restart
sleep 2
entropieF=$(cat /proc/sys/kernel/random/entropy_avail)
echo "Votre nouvelle entropie est de $entropieF bits"
if [ "$entropieF">4000 ];
	then
		echo "Entropie ok"
	else
		echo "Problème d'entropie, merci de vérifier haveged et de faire des tests additionnels avec rngtest (https://packages.debian.org/fr/stable/rng-tools)"
fi
#####################################




############### SSHD ################
sshd="/etc/ssh/sshd_config"
if [ ! -f $sshd ];
	then
		echo "${Trougeblanc}${Tgras}$sshd inexistant ! openssh-server installé ?${Treset}"
	else
		echo "${Tbleublanc}Configuration de openssh-server${Treset}"
		cp $sshd $sshd.backup #backup du fichier conf sshd
		
		grep -Pv "^[ \t]*\#.*$" $sshd.backup | #trouver les lignes non commentées
		 grep -o "\w.*" - | #enlever tout les caractères n'appartenant pas à \w (dont espaces et tabulations), 
		 sed -e "s/^/#/g" - > $sshd #commenter tout ça et le mettre dans le nouveau fichier conf
		
		
		
		conf=$lf$lf$lf
		conf+="#activer ECDH avec Curve25519 et SHA2"$lf
		conf+="KexAlgorithms curve25519-sha256@libssh.org"$lf
		conf+=$lf
		conf+="#forcer SSHd v2"$lf
		conf+="Protocol 2"$lf
		conf+=$lf
		conf+="#définir uniquement ED25519 ou RSA pour la clé publique transmise au client lors de l'échange de clés"$lf
		conf+="HostKey /etc/ssh/ssh_host_ed25519_key"$lf
		conf+="HostKey /etc/ssh/ssh_host_rsa_key"$lf
		conf+=$lf
		conf+="#désactiver l'authentification par mot de passe"$lf
		conf+="PasswordAuthentication no"$lf
		conf+="UsePAM no"$lf #pour désactiver l'authentification 
		conf+="ChallengeResponseAuthentication no"$lf
		conf+=$lf
		conf+="#activer l'authentification par clé publique"$lf
		conf+="PubkeyAuthentication yes"$lf
		conf+=$lf
		conf+="#chiffrement symétrique pour chiffrer les échanges entre client et serveur une fois l’échange de clé initial et l’authentification terminés"$lf
		conf+="#j'ai éliminé AES256-GCM car il ne chiffre pas la taille des messages, alors que chacha20 avec poly1305 si"$lf
		conf+="Ciphers chacha20-poly1305@openssh.com"$lf
		conf+="MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-512,hmac-sha2-256-etm@openssh.com,hmac-sha2-256"$lf
		echo -e $conf >> $sshd
 
 
		echo -e "${Tbleublanc}Génération des clés ${Treset}"
		#regénérer les clés publiques
		cd /etc/ssh
		rm -f ssh_host_*key*
		ssh-keygen -t ed25519 -f ssh_host_ed25519_key < /dev/null
		ssh-keygen -t rsa -b 8000 -f ssh_host_rsa_key < /dev/null
		
fi
#####################################





############### SSH ################
ssh="/etc/ssh/ssh_config"
if [ ! -f $ssh ];
	then
		echo "${Trougeblanc}$ssh inexistant ! openssh-client installé ?${Treset}"
	else
		echo "${Tbleublanc}Configuration de openssh-client${Treset}"
		cp $ssh $ssh.backup #backup du fichier conf ssh
		grep -Pv "^[ \t]*\#.*$" $ssh.backup | #trouver les lignes non commentées
		 grep -o "\w.*" - | #enlever tout les caractères n'appartenant pas à \w (dont espaces et tabulations), 
		 sed -e "s/^/#/g" - > $ssh #commenter tout ça et le mettre dans le nouveau fichier conf
		
		
		
		conf=$lf$lf$lf
		conf+="Host *"$lf
		conf+="	#activer ECDH avec Curve25519 et SHA2"$lf
		conf+="	KexAlgorithms curve25519-sha256@libssh.org"$lf
		conf+=$lf
		conf+="	#désactiver l'authentification par mot de passe"$lf
		conf+="	PasswordAuthentication no"$lf
		conf+="	ChallengeResponseAuthentication no"$lf
		conf+=$lf
		conf+="	#activer l'authentification par clé publique"$lf
		conf+="	PubkeyAuthentication yes"$lf
		conf+=$lf
		conf+="	HostKeyAlgorithms ssh-ed25519-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ssh-ed25519,ssh-rsa"$lf
		conf+="	#chiffrement symétrique pour chiffrer les échanges entre client et serveur une fois l’échange de clé initial et l’authentification terminés"$lf
		conf+="	#j'ai éliminé AES256-GCM car il ne chiffre pas la taille des messages, alors que chacha20 avec poly1305 si"$lf
		conf+="	Ciphers chacha20-poly1305@openssh.com"$lf
		conf+="	MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-512,hmac-sha2-256-etm@openssh.com,hmac-sha2-256"$lf
		conf+=""$lf
		conf+="	UseRoaming no"$lf
		echo -e $conf >> $ssh
		
		ssh-keygen -t ed25519 -o -a 100
		ssh-keygen -t rsa -b 8000 -o -a 100
		
fi
#####################################

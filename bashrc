#Décommenter l'autocomplétion dans /etc/bash.bashrc
export EDITOR='nano'
export LS_OPTIONS='--color=auto -larch'

if [ -x /usr/bin/dircolors ]; then
    eval "`dircolors`"
fi

alias ls='ls $LS_OPTIONS'
alias tailf='tail -f'
alias snano='sudo nano $1' #j'aurais pu mettre $* ou $@ : voir https://www.gnu.org/software/bash/manual/bashref.html#Positional-Parameters
alias repweb='sudo chown -R www-data:www-data /var/www/ && sudo chmod -R 770 /var/www/'
alias maj='sudo apt update && sudo apt upgrade'
alias bye='exit'
alias off='sudo poweroff'
alias sourceslist='sudo nano /etc/apt/sources.list'

pubkey() {
    gpg --keyserver pgpkeys.mit.edu --recv-key $1;
    gpg -a --export $1 | sudo apt-key add -;
    sudo apt-get update
}



#transferUP() {
#    encFile=`mktemp -t --tmpdir=/tmp/ encFileXXX`
#    openssl enc -aes-256-cbc -salt -in "$1" -out "$encFile"
#    chmod 444 $encFile
#    transferGist $encFile
#    rm -f $encFile
#}

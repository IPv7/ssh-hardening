#!/bin/bash

modif(){
	sed -i -e "s/$1/#$1/g" "$3" #commenter $1
	echo -e "$2" >> "$3" #et ajouter $2 à $3
}

#SSHD
sshd="/etc/ssh/sshd_config"
if [ ! -f $sshd ];
	then
		echo "$sshd inexistant ! SSHD installé ?"
	else
		cp $sshd $sshd.backup #backup du fichier conf sshd
		
		#activer ECDH avec Curve25519 et SHA2
		a="KexAlgorithms"
		b="KexAlgorithms curve25519-sha256@libssh.org"
		modif "$a" "$b" "$sshd"

		
		#forcer l'utilisation de SSH v2
		a="Protocol"
		b="Protocol 2"
		modif "$a" "$b" "$sshd"
		
		#définir uniquement ED25519 ou RSA pour la clé publique transmise au client
		a="HostKey"
		b="HostKey /etc/ssh/ssh_host_ed25519_key\n"
		b+="HostKey /etc/ssh/ssh_host_rsa_key"
		modif "$a" "$b" "$sshd"
		
		#regénérer les clés publiques
		cd /etc/ssh
		rm ssh_host_*key*
		ssh-keygen -t ed25519 -f ssh_host_ed25519_key < /dev/null
		ssh-keygen -t rsa -b 8000 -f ssh_host_rsa_key < /dev/null #8000 bits : AH MY GAD IT'S TOO MUCH FOR MY NOKIA 3310 !

		#désactiver l'authentification par mot de passe
		a="PasswordAuthentication"
		b="PasswordAuthentication no"
		modif "$a" "$b" "$sshd"
		#activer l'authentification par clé publique 
		a="PasswordAuthentication"
		b="PasswordAuthentication no"
		modif "$a" "$b" "$sshd"		
		
fi

#SSH
ssh="/etc/ssh/ssh_config"
if [ ! -f $ssh ];
	then
		echo "$ssh inexistant ! SSH installé ?"
	else
		cp $ssh $ssh.backup #backup du fichier conf ssh
		
		echo "KexAlgorithms curve25519-sha256@libssh.org" >> $ssh #activation d'ECDH avec Curve25519 et SHA2
fi